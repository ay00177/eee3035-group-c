<?php

error_reporting(E_ALL);


session_start();

if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true)
{
    header("location: login.php");
    exit;
}

require_once "config.php";

$carer_array = [];
$carer_names_array = [];
$carer_first = "";
$carer_last = "";
$username = $_SESSION["username"];

$sql = "SELECT carer_username
FROM carers
WHERE patient_username = ?";

if($stmt = mysqli_prepare($link,$sql))
{
    mysqli_stmt_bind_param($stmt, "s", $patient_username);
    $patient_username = $username;
    //echo "bound params";
    if(mysqli_stmt_execute($stmt))
    {
        $result = mysqli_stmt_get_result($stmt);
        while($row = mysqli_fetch_array($result, MYSQLI_NUM))
        {
            foreach($row as $r)
            {
                array_push($carer_array,$r);
            }
        }
        //echo ("success");
        //print_r($carer_array);
    }
    else
    {
        echo("Internal error - fetch carer array");
    }
    mysqli_stmt_close($stmt);
}
/*
if(mysqli_query($link,$sql))
{
    $result = mysqli_fetch_row($query);
    $current_carer = $result["carer_username"];
    echo("success");
}
else
{
    echo("Internal error");
}*/

if($carer_array != NULL)
{
    $sql = "SELECT FirstName, LastName
            FROM users
            WHERE username = ?";

    if($stmt = mysqli_prepare($link,$sql))
    {
        mysqli_stmt_bind_param($stmt, "s", $carer_username);
        foreach($carer_array as $carer_username)
        {
            if(mysqli_stmt_execute($stmt))
            {
                mysqli_stmt_bind_result($stmt,$carer_first,$carer_last);
                mysqli_stmt_fetch($stmt);
                array_push($carer_names_array, [$carer_first, $carer_last]);
                //echo ("success");
            }
            else
            {
            echo("Internal error");
            }
        }
        mysqli_stmt_close($stmt);
    }
}
else
{
    $carer_array[0] = "No carers linked";
}

?>
<html>
        <head>
            <title>Pillable</title>
            <link rel="stylesheet" type="text/css" href="styleMain.css">
        </head>
        <body>
            <div class="topnav">
                <ul>
                <img src="Pillable_short.png" alt="pillable logo" style="width:150px;height:58.1px"> 
                <a href="dashboardPatient.php">Home</a>
                <a class="active" href="PatientCarerList.php">Carer List</a>
                <a href="PatientSchedule.php">My Schedule</a>
                <a href="PatientMissed.php">Missed Doses</a>
                <a href="logout.php">Log Out</a>
                </ul>
            </div>
            <h2>Welcome <?php echo htmlspecialchars($_SESSION["FirstName"]); ?></h2>
            <div class="grandParentContaniner">
            <div class="parentContainer">
            <div class="dashboard">
            <h3> Your currently linked carers are</h3>
            <?php 
                foreach($carer_names_array as $index => $carer)
                {
                    echo "<h3>$carer[0] $carer[1] ($carer_array[$index])</h3>";
                } ?>
                <button onclick="location.href='PatientAddCarer.php';">Add a carer</button>
                <button onclick="location.href='PatientRemoveCarer.php';">Remove carer</button>
            </div>
            </div>
            </div>
        </body>
    </html>