<?php

require_once "config.php";

$username = trim($_POST["username"]);
$send_data = "";

$times_array = [];
$days = [];

$next_dose = "";
$Schedule_err = "";

$current_day = date("l");
$current_hour = date("H");
$current_minute = date("i");

$patient_array = [];
$patient_formatted = [];
$patient_first = "";
$patient_last = "";

for($i = 1; $i < 7; $i++)
{
    $day = date("l", strtotime("$current_day +$i day"));
    array_push($days, $day);
}


$sql = "SELECT patient_username
        FROM carers
        WHERE carer_username = ?";

if($stmt = mysqli_prepare($link,$sql))
{
    mysqli_stmt_bind_param($stmt, "s", $carer_username);
    $carer_username = $username;

    if(mysqli_stmt_execute($stmt))
    {
        $result = mysqli_stmt_get_result($stmt);
        while($row = mysqli_fetch_array($result, MYSQLI_NUM))
        {
            foreach ($row as $r)
            {
                array_push($patient_array,$r);
            }
        }
    }
    else
    {
        $send_data = "ERROR";
    }
    mysqli_stmt_close($stmt);
}


$patient_formatted = join("','",$patient_array);

$sql = "SELECT weekday,hour,minute,username
        FROM times
        WHERE username IN ('$patient_formatted')
        ORDER BY CASE
          WHEN weekday = ? THEN 1
          WHEN weekday =  ? THEN 2
          WHEN weekday = ? THEN 3
          WHEN weekday = ? THEN 4
          WHEN weekday = ? THEN 5
          WHEN weekday = ? THEN 6
          WHEN weekday = ? THEN 7
        END ASC, hour ASC, minute ASC";

if($stmt = mysqli_prepare($link,$sql))
{
    mysqli_stmt_bind_param($stmt, "sssssss",$current_day, $days[0], $days[1],$days[2],$days[3],$days[4],$days[5]);
    if(mysqli_stmt_execute($stmt))
    {
        $result = mysqli_stmt_get_result($stmt);
        while($row = mysqli_fetch_array($result, MYSQLI_NUM))
        {
                $times_array[] = $row;
        }
    }
    else
    {
        $send_data = "ERROR";
    }
    mysqli_stmt_close($stmt);
}

if($times_array != NULL)
{
    foreach($times_array as $dose)
    {
        if($dose[0] == $current_day)
        {
            if($dose[1] > $current_hour)
            {
                $next_dose = $time;
            }
            elseif($dose[1] == $current_hour)
            {
                if($dose[2] == $current_minute)
                {
                    $next_dose == $time;
                }
            }
        }
        else
        {
            $next_dose == $time;
        }
    }
}

if($next_dose == NULL && $times_array != NULL)
{
    $next_dose = $times_array[0];
}
else
{
    $send_data = "No Patient Schedule Set";
}

if($patient_array == NULL)
{
    $send_data = "NO PATIENTS";
}
else
{
    $sql = "SELECT FirstName, LastName
            FROM users
            WHERE username = ?";
    
    if($stmt = mysqli_prepare($link, $sql))
    {
        mysqli_stmt_bind_param($stmt, "s", $patient_username);
        $patient_username = $next_dose[3];

        if(mysqli_stmt_execute($stmt))
        {
            mysqli_stmt_bind_result($stmt,$next_patient_first,$next_patient_last);
            mysqli_stmt_fetch($stmt);
        }
        else
        {
            $send_data = "ERROR";
        }
        mysqli_stmt_close($stmt);
    }
}

if($send_data == NULL)
{
    array_push($next_dose, $next_patient_first);
    array_push($next_dose, $next_patient_last);
    $send_data = $next_dose;
}

echo json_encode($send_data);