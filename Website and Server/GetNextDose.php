<?php

require_once "config.php";

$username = trim($_POST["username"]);
$send_data = "";

$times_array = [];
$days = [];

$next_dose = "";
$Schedule_err = "";

$current_day = date("l");
$current_hour = date("H");
$current_minute = date("i");
for($i = 1; $i < 7; $i++)
{
    $day = date("l", strtotime("$current_day +$i day"));
    array_push($days, $day);
}

$sql = "SELECT weekday,hour,minute,capsule
        FROM times
        WHERE username = ?
        ORDER BY CASE
          WHEN weekday = ? THEN 1
          WHEN weekday =  ? THEN 2
          WHEN weekday = ? THEN 3
          WHEN weekday = ? THEN 4
          WHEN weekday = ? THEN 5
          WHEN weekday = ? THEN 6
          WHEN weekday = ? THEN 7
        END ASC, hour ASC, minute ASC";

if($stmt = mysqli_prepare($link,$sql))
{
    mysqli_stmt_bind_param($stmt, "ssssssss", $username_param,$current_day, $days[0], $days[1],$days[2],$days[3],$days[4],$days[5]);
    $username_param = $username;
    if(mysqli_stmt_execute($stmt))
    {
        $result = mysqli_stmt_get_result($stmt);
        while($row = mysqli_fetch_array($result, MYSQLI_NUM))
        {
                $times_array[] = $row;
        }
    }
    else
    {
        $send_data = "ERROR";
    }
    mysqli_stmt_close($stmt);
}

if($times_array != NULL)
{
    foreach($times_array as $dose)
    {
        if($dose[0] == $current_day)
        {
            if($dose[1] > $current_hour)
            {
                $next_dose = $dose;
                break;
            }
            elseif($dose[1] == $current_hour)
            {
                if($dose[2] == $current_minute)
                {
                    $next_dose = $dose;
                    break;
                }
            }
        }
        elseif($dose[0] != $current_day)
        {
            $next_dose = $dose;
            break;
        }
    }
}

if($times_array == NULL)
{
    $send_data = "No Schedule set";
}

if($next_dose == NULL && $send_data == NULL)
{
    $next_dose = $times_array[0];
}

if($send_data == NULL)
{
    $send_data = $next_dose;
}

echo json_encode($send_data);

?>