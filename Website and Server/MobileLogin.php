<?php

require_once "config.php";

$username = trim($_POST["username"]);
$password = trim($_POST["password"]);

$send_data = "";


if(empty($username))
{
    $send_data = "USERNAME BLANK";
}
if(empty($password))
{
    $send_data = "PASSWORD BLANK";
}
if(empty($send_data))
{
    $sql = "SELECT username, password, type
            FROM users
            WHERE username = ?";
                
    if($stmt = mysqli_prepare($link, $sql))
    {
        mysqli_stmt_bind_param($stmt, "s", $param_username);
        $param_username = $username;
        if(mysqli_stmt_execute($stmt))
        {
            mysqli_stmt_store_result($stmt);
            if(mysqli_stmt_num_rows($stmt) > 0)
            {
                mysqli_stmt_bind_result($stmt, $username, $password_database, $type);
                if(mysqli_stmt_fetch($stmt))
                {
                    if(password_verify($password, $password_database))
                        if($type == "patient")
                        {
                            $send_data = ["TRUE", "PATIENT"];
                        }
                        elseif($type == "carer")
                        {
                            $send_data = ["TRUE, CARER"];
                        }
                        else
                        {
                            $send_data = "ERROR";
                        }
                    else
                    {
                        $send_data = "FALSE";
                    }
                }
            }
            else
            {
                $send_data = "FALSE";
            }
        }
        else
        {
            $send_data = "ERROR";
        }
        mysqli_stmt_close($stmt); 
    }
}
mysqli_close($link); 



echo json_encode($send_data);
?>