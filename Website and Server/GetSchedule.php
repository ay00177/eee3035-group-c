<?php

require_once "config.php";

$username = (trim($_POST["username"]));
$send_data = "";

$sql = "SELECT weekday,hour,minute,capsule
        FROM times
        WHERE username = ?
        ORDER BY CASE
          WHEN weekday = \"Monday\" THEN 1
          WHEN weekday =  \"Tuesday\" THEN 2
          WHEN weekday = \"Wednesday\" THEN 3
          WHEN weekday = \"Thursday\" THEN 4
          WHEN weekday = \"Friday\" THEN 5
          WHEN weekday = \"Saturday\" THEN 6
          WHEN weekday = \"Sunday\" THEN 7
        END ASC, hour ASC, minute ASC";

if($stmt = mysqli_prepare($link,$sql))
{
    mysqli_stmt_bind_param($stmt, "s", $username_param);
    $username_param = $username;

    if(mysqli_stmt_execute($stmt))
    {
        $result = mysqli_stmt_get_result($stmt);
        while($row = mysqli_fetch_array($result, MYSQLI_NUM))
        {
                $times_array[] = $row;
                //array_push($times_array,[$individual_times]);
                //$individual_times = [];
        }
        //echo("success");
        //print_r($times_array);
        $send_data = $times_array;
    }
    else
    {
        $send_data = "ERROR";
    }
    mysqli_stmt_close($stmt);
}

if($send_data != NULL)
{
    echo json_encode($send_data);
}
else
{
    echo "No Schedule";
}

?>