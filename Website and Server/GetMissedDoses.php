<?php

require_once "config.php";

$username = trim($_POST["username"]);
$send_data = "";

$missed_array = [];

$sql = "SELECT date,hour,minute
        FROM missed
        WHERE username = ?
        ORDER BY date ASC, hour ASC, minute ASC";

if($stmt = mysqli_prepare($link,$sql))
{
    mysqli_stmt_bind_param($stmt, "s", $username_param);
    $username_param = $username;

    if(mysqli_stmt_execute($stmt))
    {
        $result = mysqli_stmt_get_result($stmt);
        while($row = mysqli_fetch_array($result, MYSQLI_NUM))
        {
            $missed_array[] = $row;
        }
    }
    else
    {
        $send_data = "ERROR";
    }
    mysqli_stmt_close($stmt);
}

if($missed_array == NULL)
{
    $send_data = "NONE MISSED";
}
if($send_data == NULL)
{
    $send_data = $missed_array;
}

echo json_encode($missed_array);

