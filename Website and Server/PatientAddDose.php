<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


session_start();

if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true)
{
    header("location: login.php");
    exit;
}

require_once "config.php";

$times_array = [];
$username = $_SESSION["username"];

$hour = "";
$hour_err = "";
$minute = "";
$minute_err = "";
$day = "";
$day_err = "";
$capsule = "";
$capsule_err = "";
$exists_err = "";
$success = "";


if($_SERVER["REQUEST_METHOD"] == "POST")
{

    $sql = "SELECT hour,minute,weekday,capsule
        FROM times
        WHERE username = ?";

    if($stmt = mysqli_prepare($link,$sql))
    {
        mysqli_stmt_bind_param($stmt, "s", $username_param);
        $username_param = $username;

        if(mysqli_stmt_execute($stmt))
        {
            $result = mysqli_stmt_get_result($stmt);
            while($row = mysqli_fetch_array($result, MYSQLI_NUM))
            {
                    $times_array[] = $row;
                    //array_push($times_array,[$individual_times]);
                    //$individual_times = [];
            }
            //echo("success");
            //print_r($times_array);
        }
        else
        {
            echo("Internal error - fetch times");
        }
        mysqli_stmt_close($stmt);
    }

    if((int)trim($_POST["hour"]) > 24 || (int)trim($_POST["hour"]) < 0)
    {
        $hour_err = "Hour must be a valid time";
    }
    else
    {
        $hour = trim($_POST["hour"]);
        //echo($hour);
    }

    if((int)trim($_POST["minute"]) > 59 || (int)trim($_POST["minute"]) < 0)
    {
        $minute_err = "Minute must be a valid time";
    }
    else
    {
        $minute = trim($_POST["minute"]);
        //echo($minute);
    }

    $day = $_POST["day"];


    if((int)trim($_POST["capsule"]) > 3 || (int)trim($_POST["capsule"]) < 0)
    {
        $capsule_err = "Capsule must be valid";
    }
    else
    {
        $capsule = trim($_POST["capsule"]);
        //echo($minute);
    }

    if(empty($hour_err) && empty($minute_err) && empty($capsule_err))
    {
        $sql = "SELECT username, hour, minute, weekday, capsule
                FROM times
                WHERE username = ?
                AND hour = ?
                AND minute = ?
                AND weekday = ?
                AND capsule = ?";
        
        if($stmt = mysqli_prepare($link, $sql))
        {
            mysqli_stmt_bind_param($stmt,"siiss", $username_param, $hour_param, $minute_param, $weekday_param, $capsule_param);
            $hour_param = $hour;
            $minute_param = $minute;
            $weekday_param = $day;
            $username_param = $username;
            $capsule_param = $capsule;
            //echo"$username_param $hour_param $minute_param $weekday_param";

            if(mysqli_stmt_execute($stmt))
            {
                mysqli_stmt_store_result($stmt);
                $result = mysqli_stmt_get_result($stmt);
                if(mysqli_stmt_num_rows($stmt) != 0)
                {
                    $exists_err = "This time is already in your schedule";
                    //echo("worked!");
                    mysqli_stmt_close($stmt);
                }
                else
                {
                    mysqli_stmt_close($stmt);

                    $sql = "INSERT INTO times (username, hour, minute, weekday,capsule)
                    VALUES (?, ?, ?, ?,?)";
            
                    if($stmt = mysqli_prepare($link, $sql))
                    {
                        mysqli_stmt_bind_param($stmt, "siiss", $param_username, $param_hour, $param_minute, $param_weekday,$param_capsule);
    
                        $param_username = $username;
                        $param_hour = $hour;
                        $param_minute = $minute;
                        $param_weekday = $day;
                        $param_capsule = $capsule;
            
                        if(mysqli_stmt_execute($stmt))
                        {
                            $success = "Time successfully added";
                        }
                        else
                        {
                            echo "Internal error - adding to schedule";
                        }
                        mysqli_stmt_close($stmt);
                    }
                }
            }
            else
            {
                echo ("Internal error - exist verify");
                mysqli_stmt_close($stmt);
            }
        }
    }
/*
    if(empty($hour_err) && empty($minute_err) && empty($day_err) && empty($exists_err))
    {
        $sql = "INSERT INTO times (username, hour, minute, weekday)
                VALUES (?, ?, ?, ?)";
        
        if($stmt = mysqli_prepare($link, $sql))
        {
            mysqli_stmt_bind_param($stmt, "siis", $param_username, $param_hour, $param_minute, $param_weekday);

            $param_username = $username;
            $param_hour = $hour;
            $param_minute = $minute;
            $param_weekday = $day;

            if(mysqli_stmt_execute($stmt))
            {
                echo "Time successfully added";
            }
            else
            {
                echo "Internal error - adding to schedule";
            }
            mysqli_stmt_close($stmt);
        }
    }*/
}

?>
<html>
    <head>
        <title>Pillable</title>
        <link rel="stylesheet" type="text/css" href="styleMain.css">
    </head>
    <body>
        <div class="topnav">
            <ul>
            <img src="Pillable_short.png" alt="pillable logo" style="width:150px;height:58.1px"> 
            <a href="dashboardPatient.php">Home</a>
            <a href="PatientCarerList.php">Carer List</a>
            <a href="PatientSchedule.php">My Schedule</a>
            <a href="PatientMissed.php">Missed Doses</a>
            <a href="logout.php">Log Out</a>
            </ul>
        </div>
        <h2>Welcome <?php echo htmlspecialchars($_SESSION["FirstName"]); ?></h2>
        <div class="grandParentContaniner">
        <div class="parentContainer">
        <div class="dashboard">
        <form method="post" action="<?= htmlspecialchars($_SERVER["eee3035/PatientAddDose.php"]);?>">
            <h3>Add a dose</h3>

            <label>Hour</label>
            <input type="text" name="hour"
                    class ="form-control <?(!empty($hour_err)) ? 'is-invalid' : ''; ?>"
                    value="<?= $hour;?>"><br>
            <span class="invalid-feedback"><?= $hour_err; ?>
            </span>


            <label>Minute</label>
            <input type="text" name="minute"
                    class ="form-control <?(!empty($minute_err)) ? 'is-invalid' : ''; ?>"
                    value="<?= $minute;?>"><br>
            <span class="invalid-feedback"><?= $minute_err; ?>
            </span>


            <label>Day</label>
            <select name="day"
                    class ="form-control <?(!empty($day_err)) ? 'is-invalid' : ''; ?>">
                <option value="Monday">Monday</option>
                <option value="Tuesday">Tuesday</option>
                <option value="Wednesday">Wednesday</option>
                <option value="Thursday">Thursday</option>
                <option value="Friday">Friday</option>
                <option value="Saturday">Saturday</option>
                <option value="Sunday">Sunday</option>
            </select>

            <br>
            <label>Capsule Number</label>
            <input type="text" name="capsule"
                    class ="form-control <?(!empty($capsule_err)) ? 'is-invalid' : ''; ?>"
                    value="<?= $capsule;?>"><br>
            <span class="invalid-feedback"><?= $capsule_err; ?>
            </span>

            <?php
                if($exists_err != "")
                {
                    echo $exists_err;
                }
                elseif($success != "")
                {
                    echo $success;
                }
            ?>
            <br>
            <button type="submit">Submit</button>
        </form>
        </div>
        </div>
        </div>
    </body>
</html>