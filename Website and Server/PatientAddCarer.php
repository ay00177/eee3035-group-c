<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true)
{
    header("location: login.php");
    exit;
}

require_once "config.php";

$carer_input = "";
$carer_error = "";
$check_type = "";
$throwaway = "";
$username = $_SESSION["username"];
$success = "";


if($_SERVER["REQUEST_METHOD"] == "POST")
{
    if(empty(trim($_POST["carer_input"])))
    {
        $carer_error = "Carer's username cannot be blank";
    }
    else
    {
        $sql = "SELECT username,type
                FROM users
                WHERE username = ?";
        if($stmt = mysqli_prepare($link, $sql))
        {
            mysqli_stmt_bind_param($stmt, "s", $carer_username);
            $carer_username = trim($_POST["carer_input"]);

            if(mysqli_stmt_execute($stmt))
            {
                mysqli_stmt_store_result($stmt);
                mysqli_stmt_bind_result($stmt,$throwaway,$check_type);
                mysqli_stmt_fetch($stmt);
                if(mysqli_stmt_num_rows($stmt) == 0)
                {
                    $carer_error = "Carer username could not be found";
                }
                elseif($check_type != "carer")
                {
                    $carer_error = "Username entered must be listed as a carer";
                }
                else
                {
                    //echo "carer err- ".$carer_error;
                    $carer_input = trim($_POST["carer_input"]);
                }
            }
            else
            {
                echo("Internal error - find carer");
            }
            mysqli_stmt_close($stmt);
        }
    }
    //echo "carer input $carer_input";
    if(empty($carer_error))
    {
        $sql = "INSERT INTO carers (patient_username, carer_username)
                VALUES (?, ?)";
        if($stmt = mysqli_prepare($link, $sql))
        {
            mysqli_stmt_bind_param($stmt, "ss", $patient_username, $carer_username);

            $carer_username = $carer_input;
            $patient_username = $username;
            //echo("success");

            if(mysqli_stmt_execute($stmt))
            {
                //echo("success");
                $success = "Success";
            }
            else
            {
                echo("Internal error - input new carer");
            }
        mysqli_stmt_close($stmt);
        }
    }
}


?>
<html>
    <head>
        <title>Pillable</title>
        <link rel="stylesheet" type="text/css" href="styleMain.css">
    </head>
    <body>
        <div class="topnav">
            <ul>
            <img src="Pillable_short.png" alt="pillable logo" style="width:150px;height:58.1px"> 
            <a href="dashboardPatient.php">Home</a>
            <a href="PatientCarerList.php">Carer List</a>
            <a href="PatientSchedule.php">My Schedule</a>
            <a href="PatientMissed.php">Missed Doses</a>
            <a href="logout.php">Log Out</a>
            </ul>
        </div>
        <h2>Welcome <?php echo htmlspecialchars($_SESSION["FirstName"]); ?></h2>
        <div class="grandParentContaniner">
        <div class="parentContainer">
        <div class="dashboard">
        <form method="post" action="<?= htmlspecialchars($_SERVER["eee3035/PatientAddCarer.php"]);?>">
            <label>Enter Carer Username</label>
            <input type = "text" name="carer_input" 
                class="carer_input"
                value="<?= $carer_input; ?>"><br><br>
            <span class="invalid-feedback"><?= $carer_error; ?> 
            </span>
            <span class="success"><?= $success; ?> 
            </span>
            <button type="submit">Submit</button>
        </form>
    </div>
    </div>
    </div>
    </body>
</html>