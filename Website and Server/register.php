<?php
    require_once "config.php";

    $username = "";
    $password = "";
    $confirm_password = "";
    $username_err = "";
    $password_err = "";
    $confirm_password_err = "";
    $FirstName = "";
    $LastName = "";
    $FirstName_err = "";
    $LastName_err = "";
    $type = "";
    $type_err = "";

    if($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if(empty(trim($_POST["username"])))
        {
            $username_err = "Username cannot be blank";
        }
        else if(!preg_match('/^[a-zA-Z0-9_]+$/', trim($_POST["username"])))
        {
            $username_err = "Username contains banned characters, please only include letters, numbers, and underscores";
        }
        else
        {
            $sql = "SELECT id, username
                    FROM users 
                    WHERE username = ?";
            
            if($stmt = mysqli_prepare($link, $sql))
            {
                mysqli_stmt_bind_param($stmt, "s", $param_username);

                $param_username = trim($_POST["username"]);

                if(mysqli_stmt_execute($stmt))
                {
                    mysqli_stmt_store_result($stmt);
                    if(mysqli_stmt_num_rows($stmt) > 0)
                    {
                        $username_err = "This username is taken";
                    }
                    else
                    {
                        $username = trim($_POST["username"]);
                    }
                }
                else
                {
                    echo("internal error");
                }
                mysqli_stmt_close($stmt);
            }
        }

        if(empty(trim($_POST["FirstName"])))
        {
            $FirstName_err = "First name cannot be blank";
        }
        else
        {
            $FirstName = trim($_POST["FirstName"]);
        }

        if(empty(trim($_POST["LastName"])))
        {
            $LastName_err = "Last name cannot be blank";
        }
        else
        {
            $LastName = trim($_POST["LastName"]);
        }

        if(empty(trim($_POST["password"])))
        {
            $password_err = "Password cannot be blank";
        }
        else if(strlen(trim($_POST["password"])) < 3)
        {
            $password_err = "Password must have at least 3 characters";
        }
        else
        {
            $password = trim($_POST["password"]);
        }

        if(empty(trim($_POST["confirm_password"])))
        {
            $confirm_password_err = "Confirm password";
        }
        else
        {
            $confirm_password = trim($_POST["confirm_password"]);
            if(empty($password_err) && ($password != $confirm_password))
            {
                $confirm_password_err = "Passwords do not match";
            }
        }

        if(empty($_POST["type"]))
        {
            $type_err = "Please select account type";
        }
        else
        {
            if($_POST["type"] == "patient")
            {
                $type = "patient";
            }
            else
            {
                $type = "carer";
            }
        }

        if(empty($username_err) && empty($password_err) && empty($confirm_password_err) && empty($FirstName_err)
        && empty($LastName_err) && empty($type_err))
        {
            $sql = "INSERT INTO users (username, password, FirstName, LastName, type) 
                    VALUES (?, ?, ?, ?, ?)";

            if($stmt = mysqli_prepare($link, $sql))
            {
                mysqli_stmt_bind_param($stmt, "sssss", $param_username, $param_password, $param_FirstName,
                 $param_LastName, $param_type);

                $param_username = $username;
                $param_password = password_hash($password, PASSWORD_DEFAULT);
                $param_FirstName = $FirstName;
                $param_LastName = $LastName;
                $param_type = $type;

                if(mysqli_stmt_execute($stmt))
                {
                    header("location: index.php");
                }
                else
                {
                    echo("Internal error - users");
                }
                
                mysqli_stmt_close($stmt);
            }
        }

        mysqli_close($link);
    }

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Pillable</title>
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body>
        <div class="grandParentContaniner">
        <div class="parentContainer">
        <form method="post" action="<?= htmlspecialchars($_SERVER["eee3035/register.php"]);?>">
        <h2>PILLABLE REGISTER</h2>

            <label>Username</label>
            <input type = "text" name="username" 
                class="form-control <?= (!empty($username_err)) ? 'is-invalid' : ''; ?>"
                value="<?= $username; ?>"><br>
            <span class="invalid-feedback"><?= $username_err; ?> 
            </span>

            <label>First Name</label>
            <input type = "text" name="FirstName" 
                class="form-control <?= (!empty($FirstName_err)) ? 'is-invalid' : ''; ?>"
                value="<?= $FirstName; ?>"><br>
            <span class="invalid-feedback"><?= $FirstName_err; ?> 
            </span>

            <label>Last Name</label>
            <input type = "text" name="LastName" 
                class="form-control <?= (!empty($LastName_err)) ? 'is-invalid' : ''; ?>"
                value="<?= $LastName; ?>"><br>
            <span class="invalid-feedback"><?= $LastName_err; ?> 
            </span>

            <label>Password</label>
            <input type="password" name="password"
                class="form-control <?= (!empty($password_err)) ? 'is-invalid' : ''; ?>" 
                value = "<?= $password; ?>"><br>
            <span class="invalid-feedback"><?= $password_err; ?> 
            </span>

            <label>Confirm Password</label>
            <input type="password" name="confirm_password" 
                class="form-control <?= (!empty($confirm_password_err)) ? 'is-invalid':''; ?>"
                value="<?= $confirm_password; ?>"><br>
            <span class = "invalid-feedback"><?= $confirm_password_err;?>
            </span>    

            
            <h3>Patient or Carer</h3>
                <div>
                    <input type="radio" id="patient" name="type" value="patient" 
                    class="form-control <?= (!empty($type_err)) ? 'is-invalid':''; ?>">
                    <label for="patient" style="display: inline-block;">Patient</label>
                </div>
                <div>
                    <input type="radio" id="carer" name="type" value="carer"
                    class="form-control <?= (!empty($type_err)) ? 'is-invalid':''; ?>">
                    <label for="carer">Carer</label>
                    <span class = "invalid-feedback"><?= $type_err;?>
                    </span> 
                </div>
            <div class="buttonHolder">
                <button type="submit">Register</button>
            </div>

            <p>Have an account? <a href="index.php">Login here</a>.</p>
            </form>
            </div>
            </div>
        </body>
    </html>