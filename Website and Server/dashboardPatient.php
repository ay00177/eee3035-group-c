<?php 
session_start();

if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true)
{
    header("location: login.php");
    exit;
}


require_once "config.php";

$times_array = [];
$username = $_SESSION["username"];
$next_dose = "";

$current_day = date("l");


$days = [];

for($i = 1; $i < 7; $i++)
{
    $day = date("l", strtotime("$current_day +$i day"));
    array_push($days, $day);
}

//echo("tomorrow $tomorrow");
//echo("weekdays - $current_day, $current_day_1, $current_day_2,$current_day_3,$current_day_4,$current_day_5,$current_day_6");

$current_hour = date("H");
$current_minute = date("i");
$Schedule_err = "";

$sql = "SELECT weekday,hour,minute,capsule
        FROM times
        WHERE username = ?
        ORDER BY CASE
          WHEN weekday = ? THEN 1
          WHEN weekday =  ? THEN 2
          WHEN weekday = ? THEN 3
          WHEN weekday = ? THEN 4
          WHEN weekday = ? THEN 5
          WHEN weekday = ? THEN 6
          WHEN weekday = ? THEN 7
        END ASC, hour ASC, minute ASC";

if($stmt = mysqli_prepare($link,$sql))
{
    mysqli_stmt_bind_param($stmt, "ssssssss", $username_param,$current_day, $days[0], $days[1],$days[2],$days[3],$days[4],$days[5]);
    $username_param = $username;
    //echo "bound params";
    if(mysqli_stmt_execute($stmt))
    {
        //echo "executed";
        $result = mysqli_stmt_get_result($stmt);
        while($row = mysqli_fetch_array($result, MYSQLI_NUM))
        {
                $times_array[] = $row;
                //array_push($times_array,[$individual_times]);
                //$individual_times = [];
        }
        //echo("success");
        //print_r($times_array);
    }
    else
    {
        echo("Internal error - fetch times");
    }
    mysqli_stmt_close($stmt);
}


if($times_array != NULL)
{
    foreach($times_array as $dose)
    {
        if($dose[0] == $current_day)
        {
            if($dose[1] > $current_hour)
            {
                $next_dose = $dose;
                break;
            }
            elseif($dose[1] == $current_hour)
            {
                if($dose[2] == $current_minute)
                {
                    $next_dose = $dose;
                    break;
                }
            }
        }
        elseif($dose[0] != $current_day)
        {
            $next_dose = $dose;
            break;
        }
    }
}


if($next_dose == NULL && $times_array != NULL)
{
    $next_dose = $times_array[0];
}
else
{
    $Schedule_err = "No Schedule Set";
}

/*
while($next_dose == NULL && $tries < 3)
{
    //echo $day_switch;
    switch($day_switch)
    {
        case "Mon":
            foreach($times_array as $time)
            {
                if($time[2] == "Mon" && $current_day == "Mon" && $counter == 0)
                {
                    if($current_hour < $time[0])
                    {
                        $next_dose = $time;
                        break;
                    }
                    elseif($current_hour == $time[0])
                    {
                        if($current_minute < $time[1])
                        {
                            $next_dose = $time;
                            break;
                        }
                    }
                }
                elseif($time[2] == "Mon")
                {
                    $next_dose = $time;
                    break;
                }
                else
                {
                    $counter = 1;
                }
            }
            break;
        case "Tue":
            foreach($times_array as $time)
            {
                //echo $time[2];
                if($time[2] == "Tue" && $current_day == "Tue" && $counter == 0)
                {
                    if($current_hour < $time[0])
                    {
                        $next_dose = $time;
                        break;
                    }
                    elseif($current_hour == $time[0])
                    {
                        if($current_minute < $time[1])
                        {
                            $next_dose = $time;
                            break;
                        }
                    }
                }
                elseif($time[2] == "Tue")
                {
                    $next_dose = $time;
                    break;
                }
            }
            $counter = 1;
            break;
        case "Wed":
            foreach($times_array as $time)
            {
                echo($time[2]);
                if($time[2] == "Wed" && $current_day == "Wed" && $counter == 0)
                {
                    if($current_hour < $time[0])
                    {
                        $next_dose = $time;
                        break;
                    }
                    elseif($current_hour == $time[0])
                    {
                        if($current_minute < $time[1])
                        {
                            $next_dose = $time;
                            break;
                        }
                    }
                }
                elseif($time[2] == "Wed")
                {
                    $next_dose = $time;
                    break;
                }
            }
            $counter = 1;
            break;
        case "Thu":
            foreach($times_array as $time)
            {
                if($time[2] == "Thu" && $current_day == "Thu" && $counter == 0)
                {
                    if($current_hour < $time[0])
                    {
                        $next_dose = $time;
                        break;
                    }
                    elseif($current_hour == $time[0])
                    {
                        if($current_minute < $time[1])
                        {
                            $next_dose = $time;
                            break;
                        }
                    }
                }
                elseif($time[2] == "Thu")
                {
                    $next_dose = $time;
                    break;
                }
            }
            $counter = 1;
            break;
        case "Fri":
            foreach($times_array as $time)
            {
                if($time[2] == "Fri" && $current_day == "Fri" && $counter == 0)
                {
                    if($current_hour < $time[0])
                    {
                        $next_dose = $time;
                        break;
                    }
                    elseif($current_hour == $time[0])
                    {
                        if($current_minute < $time[1])
                        {
                            $next_dose = $time;
                            break;
                        }
                    }
                }
                elseif($time[2] == "Fri")
                {
                    $next_dose = $time;
                    break;
                }
            }
            $counter = 1;
            break;
        case "Sat":
            foreach($times_array as $time)
            {
                if($time[2] == "Sat" && $current_day == "Sat" && $counter == 0)
                {
                    if($current_hour < $time[0])
                    {
                        $next_dose = $time;
                        break;
                    }
                    elseif($current_hour == $time[0])
                    {
                        if($current_minute < $time[1])
                        {
                            $next_dose = $time;
                            break;
                        }
                    }
                }
                elseif($time[2] == "Sat")
                {
                    $next_dose = $time;
                    break;
                }
            }
            $counter = 1;
            break;
        case "Sun":
            foreach($times_array as $time)
            {
                if($time[2] == "Sun" && $current_day == "Sun" && $counter == 0)
                {
                    if($current_hour < $time[0])
                    {
                        $next_dose = $time;
                        break;
                    }
                    elseif($current_hour == $time[0])
                    {
                        if($current_minute < $time[1])
                        {
                            $next_dose = $time;
                            break;
                        }
                    }
                }
                elseif($time[2] == "Sun")
                {
                    $next_dose = $time;
                    break;
                }
            }
            $counter = 1;
            break;
    }
    $day_switch = date("l", strtotime("$day_switch +1 day"));
    echo $next_dose;
    $tries = $tries +1;
}*/



//while($times_array[$i][3] != $current_day)

?>
<html>
        <head>
            <title>Pillable</title>
            <link rel="stylesheet" type="text/css" href="styleMain.css">
        </head>
        <body>
        <div class="topnav">
            <ul>
            <img src="Pillable_short.png" alt="pillable logo" style="width:150px;height:58.1px"> 
            <a class="active" href="dashboardPatient.php">Home</a>
            <a href="PatientCarerList.php">Carer List</a>
            <a href="PatientSchedule.php">My Schedule</a>
            <a href="PatientMissed.php">Missed Doses</a>
            <a href="logout.php">Log Out</a>
            </ul>
        </div>
        <h2>Welcome <?php echo htmlspecialchars($_SESSION["FirstName"]); ?></h2>
        <div class="grandParentContaniner">
        <div class="parentContainer">
        <div class="dashboard">
            <h3>Patient Dashboard</h3>
            <?="Today is <mark>$current_day</mark>, and the time is <mark>$current_hour:$current_minute</mark> <br><br>";?>
            <?="Your next dose is <mark>$next_dose[0] ".sprintf("%02s",$next_dose[1]).":".sprintf("%02s",$next_dose[2]) ."</mark><br>";?>
            <?="The dose should be taken from <mark>capsule $next_dose[3]</mark><br>";?>
        </div>
        </div>
        </div>
        </body>
    </html>
