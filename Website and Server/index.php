<!DOCTYPE html>
    <?php

    session_start();

    if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] == true)
    {
        if($_SESSION["type"] == "patient")
        {
            header("location: dashboardPatient.php");
            //echo ("this worked");
            exit;
        }
        elseif($_SESSION["type"] == "carer")
        {
            header("location: dashboardCarer.php");
            exit;
        }
        else
        {
            echo "Incorrect type data";
        }
    }

    require_once "config.php";

    $username = "";
    $password = "";
    $username_err = "";
    $password_err = "";
    $login_err = "";


    if($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if(empty(trim($_POST["username"])))
        {
            $username_err = "Username cannot be blank";
        }
        else
        {
            $username = trim($_POST["username"]);
        }
        if(empty(trim($_POST["password"])))
        {
            $password_err = "Password cannot be blank";
        }
        else
        {
            $password = trim($_POST["password"]);
        }
    
        if(empty($username_err) && empty($password_err))
        {
            $sql = "SELECT id, username, password, FirstName, LastName, type
                    FROM users
                    WHERE username = ?";
                    
            if($stmt = mysqli_prepare($link, $sql))
            {
                mysqli_stmt_bind_param($stmt, "s", $param_username);
                $param_username = $username;
                if(mysqli_stmt_execute($stmt))
                {
                    mysqli_stmt_store_result($stmt);
                    if(mysqli_stmt_num_rows($stmt) > 0)
                    {
                        mysqli_stmt_bind_result($stmt, $id, $username, $password_database, $FirstName, $LastName, $type);
                        if(mysqli_stmt_fetch($stmt))
                        {
                            if(password_verify($password, $password_database))
                            //if($password == $password_database)
                            {

                                session_start();
                                $_SESSION["loggedin"] = true;
                                $_SESSION["id"] = $id;
                                $_SESSION["username"] = $username;
                                $_SESSION["FirstName"] = $FirstName;
                                $_SESSION["LastName"] = $LastName;
                                $_SESSION["type"] = $type;

                                //$_SESSION["FirstName"] = mysqli_fetch_array($result)["FirstName"];
                                //$_SESSION["LastName"] = mysqli_fetch_array($result)["LastName"];
                                if($type == "patient")
                                {
                                    header("location: dashboardPatient.php");
                                    //echo ("this worked");
                                }
                                elseif($type == "carer")
                                {
                                    header("location: dashboardCarer.php");
                                }
                                else
                                {
                                    echo "Incorrect type data";
                                }
                            }
                            else
                            {
                                $login_err = "invalid username or password";
                            }
                        }
                    }
                    else
                    {
                        $login_err = "Invalid username or password";
                    }
                }
                else
                {
                    echo "We have encountered an internal error";
                }
                mysqli_stmt_close($stmt); 
            }
        }
        mysqli_close($link); 
    }

    ?>
   <html>
        <head>
            <title>Pillable</title>
            <link rel="stylesheet" type="text/css" href="style.css">
        </head>
        <body>
            <div class="grandParentContaniner">
            <div class="parentContainer">
            <image src="Pillable_long.png" alt="pillable logo" style="width:600px;height:102.3px" class="center">
            <form action="<?= htmlspecialchars($_SERVER["eee3035"]);?>" method="post">

                <?php if (!empty($login_err))
                {?>
                    <p class="error"><?= $login_err; ?></p>
                <?php } ?>

                <label>Username</label>
                <input type = "text" name="username" 
                    class="form-control <?= (!empty($username_err)) ? 'is-invalid' : ''; ?>"><br>
                    <span class="invalid-feedback"><?= $username_err; ?> 
                    </span>

                <label>Password</label>
                <input type="password" name="password"
                    class="form-control <?= (!empty($password_err)) ? 'is-invalid' : ''; ?>"><br>
                    <span class="invalid-feedback"><?= $password_err; ?> 
                    </span>

                <div class="buttonHolder">
                    <button type="submit" id="btn_l">Login</button>
                    <a href="register.php" id="btn_r">Register</a>
                </div>
            </form>
            </div>
            </div>
        </body>
    </html>
