<?php

error_reporting(E_ALL);


session_start();

if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true)
{
    header("location: login.php");
    exit;
}

require_once "config.php";

$missed_array = [];
$username = $_SESSION["username"];

$sql = "SELECT date,hour,minute
        FROM missed
        WHERE username = ?
        ORDER BY date ASC, hour ASC, minute ASC";

if($stmt = mysqli_prepare($link,$sql))
{
    mysqli_stmt_bind_param($stmt, "s", $username_param);
    $username_param = $username;

    if(mysqli_stmt_execute($stmt))
    {
        $result = mysqli_stmt_get_result($stmt);
        while($row = mysqli_fetch_array($result, MYSQLI_NUM))
        {
            $missed_array[] = $row;
        }
    }
    else
    {
        echo "Internal error - Fetch missed doses";
    }
    mysqli_stmt_close($stmt);
}

?>

<html>
        <head>
            <title>Pillable</title>
            <link rel="stylesheet" type="text/css" href="styleMain.css">
        </head>
        <body>
        <div class="topnav">
            <ul>
            <img src="Pillable_short.png" alt="pillable logo" style="width:150px;height:58.1px"> 
            <a href="dashboardPatient.php">Home</a>
            <a href="PatientCarerList.php">Carer List</a>
            <a href="PatientSchedule.php">My Schedule</a>
            <a class="active" href="PatientMissed.php">Missed Doses</a>
            <a href="logout.php">Log Out</a>
            </ul>
        </div>
        <h2>Welcome <?php echo htmlspecialchars($_SESSION["FirstName"]); ?></h2>
        <div class="grandParentContaniner">
        <div class="parentContainer">
        <div class="dashboard">
        <h3>My Missed Doses</h3>
        <?php
            if($missed_array != NULL)
            {
                echo "<table class=\"missed\">";
                    echo "<tr>";
                    echo "<th>Date</th>";
                    echo "<th>Time</th>";
                echo "</tr>";
                foreach($missed_array as $dose)
                {
                    echo "<tr>";
                        echo "<td>$dose[0]</td>";
                        echo "<td>$dose[1]:".sprintf("%02s",$dose[2])."</td>";
                }
            }
            else
            {
                echo "No doses missed";
            }
        ?>
            </div>
            </div>
            </div>
        </body>
    </html>