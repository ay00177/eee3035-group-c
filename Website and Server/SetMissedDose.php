<?php

require_once "config.php";

$username = trim($_POST["username"]);
$hour = trim($_POST["hour"]);
$minute = trim($_POST["minute"]);
$send_data = "";

$date = date("Y:m:d");

if(empty($username))
{
    $send_data = "ERROR EMPTY VARIABLE";
}

if($send_data == NULL)
{
    $sql = "INSERT INTO missed (username, date, hour, minute)
            VALUES (?, ?, ?, ?)";

    if($stmt = mysqli_prepare($link, $sql))
    {
        mysqli_stmt_bind_param($stmt, "ssii", $username_param, $date_param, $hour_param, $minute_param);
        $username_param = $username;
        $date_param = $date;
        $hour_param = $hour;
        $minute_param = $minute;

        if(mysqli_stmt_execute($stmt))
        {
            $send_data = "Success";
        }
        else
        {
            $send_data = "ERROR";
        }
        mysqli_stmt_close($stmt);
    }
}

echo $send_data;