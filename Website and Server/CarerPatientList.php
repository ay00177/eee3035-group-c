<?php


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true)
{
    header("location: login.php");
    exit;
}

require_once "config.php";

$patient_array = [];
$patient_names_array = [];
$patient_first = "";
$patient_last = "";
$username = $_SESSION["username"];


$sql = "SELECT patient_username
        FROM carers
        WHERE carer_username = ?";

if($stmt = mysqli_prepare($link,$sql))
{
    mysqli_stmt_bind_param($stmt, "s", $carer_username);
    $carer_username = $username;

    if(mysqli_stmt_execute($stmt))
    {
        $result = mysqli_stmt_get_result($stmt);
        while($row = mysqli_fetch_array($result, MYSQLI_NUM))
        {
            foreach ($row as $r)
            {
                array_push($patient_array,$r);
            }
        }
        //echo("success");
        //print_r($patient_array);
    }
    else
    {
        echo("Internal error - fetch patients");
    }
    mysqli_stmt_close($stmt);
}

if($patient_array == NULL)
{
    $patient_array[0] = "No Patients Linked";
}
else
{
    $sql = "SELECT FirstName, LastName
            FROM users
            WHERE username = ?";
    
    if($stmt = mysqli_prepare($link, $sql))
    {
        mysqli_stmt_bind_param($stmt, "s", $patient_username);
        foreach($patient_array as $patient_username)
        {
            if(mysqli_stmt_execute($stmt))
            {
                mysqli_stmt_bind_result($stmt,$patient_first,$patient_last);
                mysqli_stmt_fetch($stmt);
                array_push($patient_names_array, [$patient_first,$patient_last]);
            }
            else
            {
                echo("Internal error - Fetch patient name");
            }
            //echo $patient_names_array[0][1];

        }
        mysqli_stmt_close($stmt);
    }
}
/*
if($patient_array[0] != "No Patients Linked")
{
    foreach($patient_names_array as $patient)
}*/

?>
<html>
        <head>
            <title>Pillable</title>
            <link rel="stylesheet" type="text/css" href="styleMain.css">
        </head>
        <body>
            <div class="topnav">
                <ul>
                <img src="Pillable_short.png" alt="pillable logo" style="width:150px;height:58.1px"> 
                <a href="dashboardCarer.php">Home</a>
                <a class="active" href="CarerPatientList.php">Patient List</a>
                <a href="logout.php">Log Out</a>
                </ul>
            </div>
            <h2>Welcome <?php echo htmlspecialchars($_SESSION["FirstName"]); ?></h2>
            <div class="grandParentContaniner">
            <div class="parentContainer">
            <div class="dashboard">
            <h3>Patient List</h3>
            <?php 
                foreach($patient_names_array as $index => $patient)
                {
                    //echo $index;
                    echo "$patient[0] $patient[1] ($patient_array[$index])";
                    ?>
                    <form method="post" action="CarerPatientInfo.php">
                        <input type="hidden" name="patient_username" value="<?=$patient_array[$index]?>">
                        <button type="submit">More Information</button>
                </form><?php
                }
            ?>
        </div>
        </div>
        </div>
        </body>
</html>


