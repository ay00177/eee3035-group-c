<?php
session_start();

if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true)
{
    header("location: login.php");
    exit;
}

require_once "config.php";

$times_array = [];
$individual_times = [];
$username = $_SESSION["username"];

$sql = "SELECT hour,minute,weekday,capsule
        FROM times
        WHERE username = ?
        ORDER BY CASE
          WHEN weekday = \"Monday\" THEN 1
          WHEN weekday =  \"Tuesday\" THEN 2
          WHEN weekday = \"Wednesday\" THEN 3
          WHEN weekday = \"Thursday\" THEN 4
          WHEN weekday = \"Friday\" THEN 5
          WHEN weekday = \"Saturday\" THEN 6
          WHEN weekday = \"Sunday\" THEN 7
        END ASC, hour ASC, minute ASC";

if($stmt = mysqli_prepare($link,$sql))
{
    mysqli_stmt_bind_param($stmt, "s", $username_param);
    $username_param = $username;

    if(mysqli_stmt_execute($stmt))
    {
        $result = mysqli_stmt_get_result($stmt);
        while($row = mysqli_fetch_array($result, MYSQLI_NUM))
        {
                $times_array[] = $row;
                //array_push($times_array,[$individual_times]);
                //$individual_times = [];
        }
        //echo("success");
        //print_r($times_array);
    }
    else
    {
        echo("Internal error - fetch times");
    }
    mysqli_stmt_close($stmt);
}

?>
<html>
        <head>
            <title>Pillable</title>
            <link rel="stylesheet" type="text/css" href="styleMain.css">
        </head>
        <body>
        <div class="topnav">
            <ul>
            <img src="Pillable_short.png" alt="pillable logo" style="width:150px;height:58.1px"> 
            <a href="dashboardPatient.php">Home</a>
            <a href="PatientCarerList.php">Carer List</a>
            <a class="active" href="PatientSchedule.php">My Schedule</a>
            <a href="PatientMissed.php">Missed Doses</a>
            <a href="logout.php">Log Out</a>
            </ul>
        </div>
        <h2>Welcome <?php echo htmlspecialchars($_SESSION["FirstName"]); ?></h2>
        <div class="grandParentContaniner">
        <div class="parentContainer">
        <div class="schedule">
        <h3>My Schedule</h3>
        <?php
            if($times_array != NULL)
            {
                echo "<table class=\"sched\">";
                    echo "<tr>";
                        echo "<th>Day</th>";
                        echo "<th>Times</th>";
                        echo "<th>Capsule Number</th>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td>Monday</td>";
                        echo "<td>";
                        foreach($times_array as $time)
                        {
                            if($time[2] == "Monday")
                            {
                                echo sprintf("%02s",$time[0]).":".sprintf("%02s",$time[1]).", ";
                            }
                        }
                        echo "</td>";
                        echo "<td>";
                        foreach($times_array as $time)
                        {
                            if($time[2] == "Monday")
                            {
                                echo "$time[3], ";
                            }
                        }
                        echo "</td>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td>Tuesday</td>";
                        echo "<td>";
                        foreach($times_array as $time)
                        {
                            if($time[2] == "Tuesday")
                            {
                                echo sprintf("%02s",$time[0]).":".sprintf("%02s",$time[1]).", ";
                            }
                        }
                        echo "</td>";
                        echo "<td>";
                        foreach($times_array as $time)
                        {
                            if($time[2] == "Tuesday")
                            {
                                echo "$time[3], ";
                            }
                        }
                        echo "</td>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td>Wednesday</td>";
                        echo "<td>";
                        foreach($times_array as $time)
                        {
                            if($time[2] == "Wednesday")
                            {
                                echo sprintf("%02s",$time[0]).":".sprintf("%02s",$time[1]).", ";
                            }
                        }
                        echo "</td>";
                        echo "<td>";
                        foreach($times_array as $time)
                        {
                            if($time[2] == "Wednesday")
                            {
                                echo "$time[3], ";
                            }
                        }
                        echo "</td>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td>Thursday</td>";
                        echo "<td>";
                        foreach($times_array as $time)
                        {
                            if($time[2] == "Thursday")
                            {
                                echo sprintf("%02s",$time[0]).":".sprintf("%02s",$time[1]).", ";
                            }
                        }
                        echo "</td>";
                        echo "<td>";
                        foreach($times_array as $time)
                        {
                            if($time[2] == "Thursday")
                            {
                                echo "$time[3], ";
                            }
                        }
                        echo "</td>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td>Friday</td>";
                        echo "<td>";
                        foreach($times_array as $time)
                        {
                            if($time[2] == "Friday")
                            {
                                echo sprintf("%02s",$time[0]).":".sprintf("%02s",$time[1]).", ";
                            }
                        }
                        echo "</td>";
                        echo "<td>";
                        foreach($times_array as $time)
                        {
                            if($time[2] == "Friday")
                            {
                                echo "$time[3], ";
                            }
                        }
                        echo "</td>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td>Saturday</td>";
                        echo "<td>";
                        foreach($times_array as $time)
                        {
                            if($time[2] == "Saturday")
                            {
                                echo sprintf("%02s",$time[0]).":".sprintf("%02s",$time[1]).", ";
                            }
                        }
                        echo "<td>";
                        foreach($times_array as $time)
                        {
                            if($time[2] == "Saturday")
                            {
                                echo "$time[3], ";
                            }
                        }
                        echo "</td>";
                        echo "</td>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td>Sunday</td>";
                        echo "<td>";
                        foreach($times_array as $time)
                        {
                            if($time[2] == "Sunday")
                            {
                                echo sprintf("%02s",$time[0]).":".sprintf("%02s",$time[1]).", ";
                            }
                        }
                        echo "</td>";
                        echo "<td>";
                        foreach($times_array as $time)
                        {
                            if($time[2] == "Sunday")
                            {
                                echo "$time[3], ";
                            }
                        }
                        echo "</td>";
                    echo "</tr>";
                echo "</table>";
            }
            else
            {
                echo "<h3> No schedule set </h3>";
            }
        ?>
            <br>
            <button onclick="location.href='PatientAddDose.php';">Add a dose</button>
            <button onclick="location.href='PatientRemoveDose.php';">Remove a dose</button>
            </div>
            </div>
            </div>
            </div>
        </body>
    </html>
