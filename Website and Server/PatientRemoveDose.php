<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


session_start();

if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true)
{
    header("location: login.php");
    exit;
}

require_once "config.php";

$times_array = [];
$username = $_SESSION["username"];

$DoseSelect = "";
$DoseErr = "";
$deleted = "";


$sql = "SELECT weekday,hour,minute
        FROM times
        WHERE username = ?
        ORDER BY CASE
          WHEN weekday = \"Monday\" THEN 1
          WHEN weekday =  \"Tuesday\" THEN 2
          WHEN weekday = \"Wednesday\" THEN 3
          WHEN weekday = \"Thursday\" THEN 4
          WHEN weekday = \"Friday\" THEN 5
          WHEN weekday = \"Saturday\" THEN 6
          WHEN weekday = \"Sunday\" THEN 7
        END ASC, hour ASC, minute ASC";

if($stmt = mysqli_prepare($link,$sql))
{
    mysqli_stmt_bind_param($stmt, "s", $username_param);
    $username_param = $username;
    //echo "bound params";
    if(mysqli_stmt_execute($stmt))
    {
        $result = mysqli_stmt_get_result($stmt);
        while($row = mysqli_fetch_array($result, MYSQLI_NUM))
        {
                $times_array[] = $row;
                //array_push($times_array,[$individual_times]);
                //$individual_times = [];
        }
        //echo("success");
        //print_r($times_array);
    }
    else
    {
        echo("Internal error - fetch times");
    }
    mysqli_stmt_close($stmt);
}

if($_SERVER["REQUEST_METHOD"] == "POST")
{
    if(empty(trim($_POST["DoseSelect"])))
    {
        $DoseErr = "Please select a dose";
    }
    else
    {
        $sql = "DELETE FROM times
                WHERE username = ?
                AND hour = ?
                AND minute = ?
                AND weekday = ?";
            
        if($stmt = mysqli_prepare($link, $sql))
        {
            mysqli_stmt_bind_param($stmt, "siis", $username_param, $hour_param, $minute_param, $weekday_param);
                
            foreach($_POST["DoseSelect"] as $i)
            {
                //echo "within foreach";
                $username_param = $username;
                $hour_param = $times_array[$i][1];
                $minute_param = $times_array[$i][2];
                $weekday_param = $times_array[$i][0];

                if(mysqli_stmt_execute($stmt))
                {
                    $deleted = "Successfully deleted dose";
                }
                else
                {
                    echo("Internal error - Delete dose");
                }
            }
            mysqli_stmt_close($stmt);
        }
    }
}


?>
<html>
    <head>
        <title>Pillable</title>
        <link rel="stylesheet" type="text/css" href="styleMain.css">
    </head>
    <body>
        <div class="topnav">
            <ul>
            <img src="Pillable_short.png" alt="pillable logo" style="width:150px;height:58.1px"> 
            <a href="dashboardPatient.php">Home</a>
            <a href="PatientCarerList.php">Carer List</a>
            <a href="PatientSchedule.php">My Schedule</a>
            <a href="PatientMissed.php">Missed Doses</a>
            <a href="logout.php">Log Out</a>
            </ul>
        </div>
        <h2>Welcome <?php echo htmlspecialchars($_SESSION["FirstName"]); ?></h2>
        <div class="grandParentContaniner">
        <div class="parentContainer">
        <div class="dashboard">
        <form method="post" action="<?= htmlspecialchars($_SERVER["eee3035/PatientRemoveDose.php"]);?>">
            <h3>Remove Dose</h3>
            <?php
            foreach($times_array as $index => $dose)
            {
                $label = "$dose[0] ".sprintf("%02s",$dose[1]).":".sprintf("%02s",$dose[2]);?>
                <input type="checkbox" name="DoseSelect[]" value="<?=$index;?>"><?=$label;?></input>
                <br>
                <?php
            }?>
            <span class = "invalid-feedback"><?= $DoseErr;?>
            </span>
            <span class = "success"><?= $deleted;?>
            </span><br>
            <button type="submit">Submit</button>
        </form>
        </div>
        </div>
        </div>
    </body>
</html>