<?php

$current_day = date("l");
$current_date = date("Y:m:d"); 
$current_hour = date("H");
$current_minute = date("i");
$current_second = date("s");

$send_data = [$current_date, $current_hour, $current_minute, $current_second];

echo json_encode($send_data);

?>