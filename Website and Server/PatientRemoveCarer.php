<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


session_start();

if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true)
{
    header("location: login.php");
    exit;
}

require_once "config.php";

$carer_array = [];
$carer_names_array = [];
$carer_first = "";
$carer_last = "";
$username = $_SESSION["username"];


$sql = "SELECT carer_username
FROM carers
WHERE patient_username = ?";

if($stmt = mysqli_prepare($link,$sql))
{
    mysqli_stmt_bind_param($stmt, "s", $patient_username);
    $patient_username = $username;
    //echo "bound params";
    if(mysqli_stmt_execute($stmt))
    {
        $result = mysqli_stmt_get_result($stmt);
        while($row = mysqli_fetch_array($result, MYSQLI_NUM))
        {
            foreach($row as $r)
            {
                array_push($carer_array,$r);
            }
        }
        //echo ("success");
        //print_r($carer_array);
    }
    else
    {
        echo("Internal error - fetch carer array");
    }
    mysqli_stmt_close($stmt);
}

if($carer_array != NULL)
{
    $sql = "SELECT FirstName, LastName
            FROM users
            WHERE username = ?";

    if($stmt = mysqli_prepare($link,$sql))
    {
        mysqli_stmt_bind_param($stmt, "s", $carer_username);
        foreach($carer_array as $carer_username)
        {
            if(mysqli_stmt_execute($stmt))
            {
                mysqli_stmt_bind_result($stmt,$carer_first,$carer_last);
                mysqli_stmt_fetch($stmt);
                array_push($carer_names_array, [$carer_first, $carer_last]);
                //echo ("success");
            }
            else
            {
            echo("Internal error");
            }
            //print_r($carer_names_array);
        }
        mysqli_stmt_close($stmt);
    }
}
else
{
    $carer_array[0] = "No carers linked";
}

if($carer_array[0] != "No carers linked")
{
    foreach($carer_array as $index => $carer)
    {
        array_push($carer_names_array[$index], $carer);
    }
}

//print_r($carer_names_array);

if($_SERVER["REQUEST_METHOD"] == "POST")
{
    if(empty($_POST["carerSelect"]))
    {
        $carerErr = "Please select a carer to remove";
    }
    else
    {
        $sql = "DELETE FROM carers
                WHERE patient_username = ?
                AND carer_username = ?";
        
        if($stmt = mysqli_prepare($link, $sql))
        {
            mysqli_stmt_bind_param($stmt, "ss", $patient_username, $carer_username);
            
            foreach($_POST["carerSelect"] as $i)
            {
                //echo "within foreach";
                $patient_username = $username;
                $carer_username = $carer_array[$i];

                if(mysqli_stmt_execute($stmt))
                {
                    $deleted = "Successfully removed carer";
                }
                else
                {
                    echo("Internal error - remove carer");
                }
            }
            mysqli_stmt_close($stmt);
        }
    }
}
?>

<html>
        <head>
            <title>Pillable</title>
            <link rel="stylesheet" type="text/css" href="styleMain.css">
        </head>
        <body>
            <div class="topnav">
                <ul>
                <img src="Pillable_short.png" alt="pillable logo" style="width:150px;height:58.1px"> 
                <a href="dashboardPatient.php">Home</a>
                <a href="PatientCarerList.php">Carer List</a>
                <a href="PatientSchedule.php">My Schedule</a>
                <a href="PatientMissed.php">Missed Doses</a>
                <a href="logout.php">Log Out</a>
                </ul>
            </div>
            <h2>Welcome <?php echo htmlspecialchars($_SESSION["FirstName"]); ?></h2>
            <div class="grandParentContaniner">
            <div class="parentContainer">
            <div class="dashboard">
            <form method="post" action="<?= htmlspecialchars($_SERVER["eee3035/PatientRemoveCarer.php"]);?>">
            <h3>Remove Carer</h3>
            <?php
            foreach($carer_names_array as $index => $carer)
            {
                $label = "$carer[0] $carer[1] ($carer[2])";?>
                <input type="checkbox" name="carerSelect[]" value="<?=$index;?>"><?=$label;?></input>
                <br>
                <?php
            }?>
            <span class = "invalid-feedback"><?= $DoseErr;?>
            </span>
            <span class = "success"><?= $deleted;?>
            </span><br>
            <button type="submit">Submit</button>
        </form>
        </div>
        </div>
        </div>
        </body>
</html>