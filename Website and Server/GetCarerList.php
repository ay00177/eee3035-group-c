<?php

require_once "config.php";

$username = trim($_POST["username"]);
$send_data = "";

$carer_array = [];
$carer_names_array = [];
$carer_first = "";
$carer_last = "";

$sql = "SELECT carer_username
FROM carers
WHERE patient_username = ?";


if($stmt = mysqli_prepare($link,$sql))
{
    mysqli_stmt_bind_param($stmt, "s", $patient_username);
    $patient_username = $username;
    if(mysqli_stmt_execute($stmt))
    {
        $result = mysqli_stmt_get_result($stmt);
        while($row = mysqli_fetch_array($result, MYSQLI_NUM))
        {
            foreach($row as $r)
            {
                array_push($carer_array,$r);
            }
        }
    }
    else
    {
        $send_data = "ERROR";
    }
    mysqli_stmt_close($stmt);
}


if($carer_array != NULL)
{
    $sql = "SELECT FirstName, LastName
            FROM users
            WHERE username = ?";

    if($stmt = mysqli_prepare($link,$sql))
    {
        mysqli_stmt_bind_param($stmt, "s", $carer_username);
        foreach($carer_array as $carer_username)
        {
            if(mysqli_stmt_execute($stmt))
            {
                mysqli_stmt_bind_result($stmt,$carer_first,$carer_last);
                mysqli_stmt_fetch($stmt);
                array_push($carer_names_array, [$carer_first, $carer_last]);
            }
            else
            {
                $send_data = "ERROR";
            }
        }
        mysqli_stmt_close($stmt);
    }
}
else
{
    $send_data = "NO CARERS";
}


if($send_data == NULL)
{
    foreach($carer_array as $index => $carer)
    {
        //$send_data[] = [$patient[0], $patient[1], $patient_array[$index]];
        array_push($carer_names_array[$index], $carer);
    }
    $send_data = $carer_names_array;
}

echo json_encode($send_data);
?>