<?php
session_start();

if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true)
{
    header("location: login.php");
    exit;
}

require_once "config.php";

$times_array = [];
$username = $_SESSION["username"];
$next_dose = "";

$current_day = date("l");

$days = [];

$current_hour = date("H");
$current_minute = date("i");
$Schedule_err = "";

$patient_array = [];
$patient_formatted = [];
$patient_first = "";
$patient_last = "";

for($i = 1; $i < 7; $i++)
{
    $day = date("l", strtotime("$current_day +$i day"));
    array_push($days, $day);
}

$sql = "SELECT patient_username
        FROM carers
        WHERE carer_username = ?";

if($stmt = mysqli_prepare($link,$sql))
{
    mysqli_stmt_bind_param($stmt, "s", $carer_username);
    $carer_username = $username;

    if(mysqli_stmt_execute($stmt))
    {
        $result = mysqli_stmt_get_result($stmt);
        while($row = mysqli_fetch_array($result, MYSQLI_NUM))
        {
            foreach ($row as $r)
            {
                array_push($patient_array,$r);
            }
        }
        //echo("success");
        //print_r($patient_array);
    }
    else
    {
        echo("Internal error - fetch patients");
    }
    mysqli_stmt_close($stmt);
}

$patient_formatted = join("','",$patient_array);
//print_r($patient_formatted);

$sql = "SELECT weekday,hour,minute,username
        FROM times
        WHERE username IN ('$patient_formatted')
        ORDER BY CASE
          WHEN weekday = ? THEN 1
          WHEN weekday =  ? THEN 2
          WHEN weekday = ? THEN 3
          WHEN weekday = ? THEN 4
          WHEN weekday = ? THEN 5
          WHEN weekday = ? THEN 6
          WHEN weekday = ? THEN 7
        END ASC, hour ASC, minute ASC";

if($stmt = mysqli_prepare($link,$sql))
{
    mysqli_stmt_bind_param($stmt, "sssssss",$current_day, $days[0], $days[1],$days[2],$days[3],$days[4],$days[5]);
    //echo "bound params";
    if(mysqli_stmt_execute($stmt))
    {
        //echo "executed";
        $result = mysqli_stmt_get_result($stmt);
        while($row = mysqli_fetch_array($result, MYSQLI_NUM))
        {
                $times_array[] = $row;
                //array_push($times_array,[$individual_times]);
                //$individual_times = [];
        }
        //echo("success");
        //print_r($times_array);
    }
    else
    {
        echo("Internal error - fetch times");
    }
    mysqli_stmt_close($stmt);
}

if($times_array != NULL)
{
    foreach($times_array as $dose)
    {
        if($dose[0] == $current_day)
        {
            if($dose[1] > $current_hour)
            {
                $next_dose = $dose;
                break;
            }
            elseif($dose[1] == $current_hour)
            {
                if($dose[2] == $current_minute)
                {
                    $next_dose = $dose;
                    break;
                }
            }
        }
        else
        {
            $next_dose = $dose;
            break;
        }
    }
}

if($next_dose == NULL && $times_array != NULL)
{
    $next_dose = $times_array[0];
}
else
{
    $Schedule_err = "No Patient Schedule Set";
}



if($patient_array == NULL)
{
    $patient_array[0] = "No Patients Linked";
}
else
{
    $sql = "SELECT FirstName, LastName
            FROM users
            WHERE username = ?";
    
    if($stmt = mysqli_prepare($link, $sql))
    {
        mysqli_stmt_bind_param($stmt, "s", $patient_username);
        $patient_username = $next_dose[3];

        if(mysqli_stmt_execute($stmt))
        {
            mysqli_stmt_bind_result($stmt,$next_patient_first,$next_patient_last);
            mysqli_stmt_fetch($stmt);
        }
        else
        {
            echo("Internal error - Fetch patient name");
        }
        //echo $patient_names_array[0][1];
        mysqli_stmt_close($stmt);
    }
}


?>
<html>
        <head>
            <title>Pillable</title>
            <link rel="stylesheet" type="text/css" href="styleMain.css">
        </head>
        <body>
        <div class="topnav">
            <ul>
            <img src="Pillable_short.png" alt="pillable logo" style="width:150px;height:58.1px"> 
            <a class="active" href="dashboardCarer.php">Home</a>
            <a href="CarerPatientList.php">Patient List</a>
            <a href="logout.php">Log Out</a>
            </ul>
        </div>
        <h2>Welcome <?php echo htmlspecialchars($_SESSION["FirstName"]); ?></h2>
        <div class="grandParentContaniner">
        <div class="parentContainer">
        <div class="dashboard">
            <h3>Carer Dashboard</h3>
            <?="Today is <mark>$current_day</mark>, and the time is <mark>$current_hour:$current_minute</mark> <br>";?>
            <?="Your next patient awaiting a dose is <mark>$next_patient_first $next_patient_last ($next_dose[3])</mark>
                <br> on <mark>$next_dose[0]</mark> at <mark>".sprintf("%02s",$next_dose[1]).":".sprintf("%02s",$next_dose[2]) ."</mark><br>";?>
        </div>
        </div>
        </div>
        </body>
    </html>