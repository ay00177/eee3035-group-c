<?php

require_once "config.php";

$username = trim($_POST["username"]);
$send_data = "";

$patient_array = [];
$patient_names_array = [];
$patient_first = "";
$patient_last = "";

$sql = "SELECT patient_username
        FROM carers
        WHERE carer_username = ?";

if($stmt = mysqli_prepare($link,$sql))
{
    mysqli_stmt_bind_param($stmt, "s", $carer_username);
    $carer_username = $username;

    if(mysqli_stmt_execute($stmt))
    {
        $result = mysqli_stmt_get_result($stmt);
        while($row = mysqli_fetch_array($result, MYSQLI_NUM))
        {
            foreach ($row as $r)
            {
                array_push($patient_array,$r);
            }
        }
    }
    else
    {
        $send_data = "ERROR";
    }
    mysqli_stmt_close($stmt);
}

//print_r($patient_array);

if($patient_array == NULL)
{
    $send_data = "NO PATIENTS";
}
else
{
    $sql = "SELECT FirstName, LastName
            FROM users
            WHERE username = ?";
    
    if($stmt = mysqli_prepare($link, $sql))
    {
        mysqli_stmt_bind_param($stmt, "s", $patient_username);
        foreach($patient_array as $patient_username)
        {
            if(mysqli_stmt_execute($stmt))
            {
                mysqli_stmt_bind_result($stmt,$patient_first,$patient_last);
                mysqli_stmt_fetch($stmt);
                array_push($patient_names_array, [$patient_first,$patient_last]);
            }
            else
            {
                $send_data = "ERROR";
            }
        }
        mysqli_stmt_close($stmt);
    }
}

//print_r($patient_names_array);
//print_r($send_data);

if($send_data == NULL)
{
    foreach($patient_array as $index => $patient)
    {
        //$send_data[] = [$patient[0], $patient[1], $patient_array[$index]];
        array_push($patient_names_array[$index], $patient);
    }
    $send_data = $patient_names_array;
}

echo json_encode($send_data);

?>