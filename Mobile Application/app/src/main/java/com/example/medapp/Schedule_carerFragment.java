package com.example.medapp;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.medapp.databinding.FragmentScheduleCarerBinding;

import java.util.HashMap;
import java.util.Map;


public class Schedule_carerFragment extends Fragment {
    private FragmentScheduleCarerBinding binding;
    RequestQueue queue;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        queue = Volley.newRequestQueue(this.getContext());
        binding = FragmentScheduleCarerBinding.inflate(inflater, container, false);
        queue.add(scheduleRequest(DataSingleton.getInstance().selectedPatient));
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.patientNameTextview.setText(DataSingleton.getInstance().selectedPatient);
        binding.backCarerScheduleBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                NavHostFragment.findNavController(Schedule_carerFragment.this).popBackStack();
            }

        });
    }

    StringRequest scheduleRequest(String username) {
        String Url = "https://skinnerserver.ddns.net/eee3035/GetSchedule.php";

        return new StringRequest(Request.Method.POST, Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("VOLLEY", response);

                String[] resp = AppUtils.parseResp(response);
                for (int i = 0; i < resp.length; i = i + 4) {
                    switch (resp[i].replace("\"","")) {
                        case "Monday":
                            binding.textView26.setText(resp[i+1] + " " +resp[i+2]);
                            break;
                        case "Tuesday":
                            binding.textView27.setText(resp[i+1] + " " +resp[i+2]);
                            break;
                        case "Wednesday":
                            binding.textView32.setText(resp[i+1] + " " +resp[i+2]);
                            break;
                        case "Thursday":
                            binding.textView28.setText(resp[i+1] + " " +resp[i+2]);
                            break;
                        case "Friday":
                            binding.textView29.setText(resp[i+1] + " " +resp[i+2]);
                            break;
                        case "Saturday":
                            binding.textView30.setText(resp[i+1] + " " +resp[i+2]);
                            break;
                        case "Sunday":
                            binding.textView31.setText(resp[i+1] + " " +resp[i+2]);
                            break;
                    }
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                return params;
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}