package com.example.medapp;

import static android.content.Context.ALARM_SERVICE;
import static androidx.core.content.ContextCompat.getSystemService;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.TemporalField;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.medapp.databinding.FragmentHomeBinding;

import org.json.JSONException;
import org.json.JSONObject;

public class HomeFragment extends Fragment {

    private FragmentHomeBinding binding;
    private  Thread t;
    private LocalDateTime time;
    RequestQueue queue;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
         queue = Volley.newRequestQueue(this.getContext());
        binding = FragmentHomeBinding.inflate(inflater, container, false);
        createNotificationChannel();
        t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateTextView(time);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        t.start();

        return binding.getRoot();

    }
    private void updateTextView(LocalDateTime time ) {
        if (time == null){
            if(binding!= null)
                binding.timeUntilNextDoseValue.setText("----");
            return;
        }
        Date nextDose = Date.from(time.atZone(ZoneId.systemDefault()).toInstant());
        Date currentTime = Calendar.getInstance().getTime();
        long timeDiff = nextDose.getTime() - currentTime.getTime();
        long hours = TimeUnit.HOURS.convert(timeDiff, TimeUnit.MILLISECONDS);
        long mins = (timeDiff / 60000) % 60;
        long sec = (timeDiff / 1000) % 60;
        if(binding!= null)
            binding.timeUntilNextDoseValue.setText(hours + ":" + mins + ":" + sec);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        queue.add(nextDoseRequest(DataSingleton.getInstance().username));
        binding.carerDetatilsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(HomeFragment.this)
                        .navigate(R.id.action_FirstFragment_to_CarerListFragment);
            }
        });
        binding.doseScheduleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(HomeFragment.this)
                        .navigate(R.id.action_home_fragment_to_scheduleFragment);
            }
        });

    }

    @Override
    public void onDestroyView() {
        t.interrupt();
        super.onDestroyView();
        binding = null;
    }

    StringRequest nextDoseRequest(String username) {
        String Url = "https://skinnerserver.ddns.net/eee3035/GetNextDose.php";

        return new StringRequest(Request.Method.POST, Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("VOLLEY", response);

                String[] resp = AppUtils.parseResp(response);
                if(resp.length == 1)
                {
                    binding.nextDoseValue.setText( response.replace("\"",""));
                }
                if(resp.length == 4){
                    //response... :/
                    int day =0;
                    switch (resp[0]) {
                        case "Monday":
                            day=1;
                            break;
                        case "Tuesday":
                            day=2;
                            break;
                        case "Wednesday":
                            day=3;
                            break;
                        case "Thursday":
                            day=4;
                            break;
                        case "Friday":
                            day=5;
                            break;
                        case "Saturday":
                            day=6;
                            break;
                        case "Sunday":
                            day=7;
                            break;
                    }
                    Calendar calendar = Calendar.getInstance();
                    int currentDay = calendar.get(Calendar.DAY_OF_WEEK)-1;
                    if(currentDay == 0) // thanks America
                        currentDay = 7;

                    binding.nextDoseValue.setText( resp[0].replace("\"","") +" " + resp[1] + " " + resp[2]);
                    Date currentDate = new Date();
                    LocalDateTime localDateTime = currentDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
                    time = localDateTime.plusDays(day - currentDay).withHour( Integer.parseInt(resp[1])).withMinute(Integer.parseInt(resp[2]));
                    Intent intent = new Intent( getActivity(),ReminderBroadcast.class);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(),0,intent,PendingIntent.FLAG_IMMUTABLE);
                    AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(ALARM_SERVICE);

                    alarmManager.set(AlarmManager.RTC_WAKEUP,time.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli(),pendingIntent);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                return params;
            }
        };
    }
    void createNotificationChannel(){

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            CharSequence name ="MedReminderChannel";
            String description = "Channel for med reminder";
            NotificationChannel channel = new NotificationChannel("notifyMed",name, NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(description);

            NotificationManager notificationManager = this.getContext().getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

}