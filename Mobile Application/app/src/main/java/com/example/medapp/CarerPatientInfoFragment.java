package com.example.medapp;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.fragment.NavHostFragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.medapp.databinding.FragmentCarerPatientInfoBinding;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class CarerPatientInfoFragment extends Fragment {

    private FragmentCarerPatientInfoBinding binding;
    private Thread t;
    RequestQueue queue;
    private LocalDateTime time;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        queue = Volley.newRequestQueue(this.getContext());

        binding = FragmentCarerPatientInfoBinding.inflate(inflater, container, false);

         t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        FragmentActivity fa = getActivity();
                        if(fa != null)
                            fa.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateTextView();
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        t.start();

        return binding.getRoot();

    }

    private void updateTextView() {
        if (time == null){
            if(binding!= null)
                binding.timeUntilNextDoseValue2.setText("----");
            return;
        }
        Date nextDose = Date.from(time.atZone(ZoneId.systemDefault()).toInstant());
        Date currentTime = Calendar.getInstance().getTime();
        long timeDiff = nextDose.getTime() - currentTime.getTime();
        long hours = TimeUnit.HOURS.convert(timeDiff, TimeUnit.MILLISECONDS);
        long mins = (timeDiff / 60000) % 60;
        long sec = (timeDiff / 1000) % 60;
        if( binding.timeUntilNextDoseValue2 !=null)
            binding.timeUntilNextDoseValue2.setText(hours + ":" + mins + ":" + sec);
    }
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        queue.add(nextDoseRequest(DataSingleton.getInstance().selectedPatient));
        binding.patientName.setText(DataSingleton.getInstance().getSelectedPatient());
        binding.nextDoseValue2.setText("Wednesday 7pm");

        binding.backBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                NavHostFragment.findNavController(CarerPatientInfoFragment.this).popBackStack();
            }

        });
        binding.doseScheduleBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                NavHostFragment.findNavController(CarerPatientInfoFragment.this).navigate(R.id.action_patient_info_fragment_to_schedule_carerFragment);
            }

        });
        binding.historyButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                NavHostFragment.findNavController(CarerPatientInfoFragment.this).navigate(R.id.action_patient_info_fragment_to_historyFragment);;
            }

        });
    }

    StringRequest nextDoseRequest(String username) {
        String Url = "https://skinnerserver.ddns.net/eee3035/GetNextDose.php";

        return new StringRequest(Request.Method.POST, Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("VOLLEY", response);

                String[] resp = AppUtils.parseResp(response);
                if(resp.length == 1)
                {
                    binding.nextDoseValue2.setText( response.replace("\"",""));
                }
                if(resp.length == 4){
                    //response... :/
                    int day =0;
                    switch (resp[0]) {
                        case "Monday":
                            day=1;
                            break;
                        case "Tuesday":
                            day=2;
                            break;
                        case "Wednesday":
                            day=3;
                            break;
                        case "Thursday":
                            day=4;
                            break;
                        case "Friday":
                            day=5;
                            break;
                        case "Saturday":
                            day=6;
                            break;
                        case "Sunday":
                            day=7;
                            break;
                    }
                    Calendar calendar = Calendar.getInstance();
                    int currentDay = calendar.get(Calendar.DAY_OF_WEEK)-1;
                    if(currentDay == 0) // thanks America
                        currentDay = 7;

                    binding.nextDoseValue2.setText( resp[0].replace("\"","") +" " + resp[1] + " " + resp[2]);
                    Date currentDate = new Date();
                    LocalDateTime localDateTime = currentDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
                    time= localDateTime.plusDays(day - currentDay).withHour( Integer.parseInt(resp[1])).withMinute(Integer.parseInt(resp[2]));
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                return params;
            }
        };
    }

    @Override
    public void onDestroyView() {

        super.onDestroyView();
        t.interrupt();
        binding = null;
    }

}