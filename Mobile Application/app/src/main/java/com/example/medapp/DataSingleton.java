package com.example.medapp;

public class DataSingleton {
    String userType;
    String username;
    String selectedPatient;
    private static final DataSingleton ourInstance = new DataSingleton();
    public static DataSingleton getInstance() {
        return ourInstance;
    }
    private DataSingleton() {
    }

    public void setUserType(String t) {
        this.userType = t;
    }
    public String getUserType() {
        return userType;
    }
    public void setSelectedPatient(String t) {
        this.selectedPatient = t;
    }
    public String getSelectedPatient() {
        return selectedPatient;
    }
    public void setUsername(String s){this.username = s;}
    public String getUsername(){return  username;}
}