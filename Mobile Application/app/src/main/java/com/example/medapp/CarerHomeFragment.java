package com.example.medapp;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.medapp.databinding.FragmentCarerHomeBinding;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CarerHomeFragment extends Fragment {

    private FragmentCarerHomeBinding binding;
    RequestQueue queue;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        queue = Volley.newRequestQueue(this.getContext());
        binding = FragmentCarerHomeBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        queue.add(patienList(DataSingleton.getInstance().username));
//        String[] names = {"Steven Lastname","Jason Something","Someone else"};
//        for (int i = 0; i < names.length; i++) {
//            TextView valueTV = new TextView(getContext());
//            valueTV.setText(names[i]);
//            valueTV.setTextSize(40);
//            final String name = names[i];
//            valueTV.setOnClickListener(new View.OnClickListener(){
//                @Override
//                public void onClick(View v){
//                    DataSingleton.getInstance().setSelectedPatient(name);
//                    NavHostFragment.findNavController(CarerHomeFragment.this)
//                            .navigate(R.id.carer_to_info_patient);
//                }
//
//            });
//            binding.listPatient.addView(valueTV);
//        }

    }
    StringRequest patienList(String username) {
        String Url = "https://skinnerserver.ddns.net/eee3035/GetPatientList.php";

        return new StringRequest(Request.Method.POST, Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("VOLLEY", response);

                String[] resp = AppUtils.parseResp(response);
                for (int i = 0; i < resp.length; i = i + 3) {
                    TextView valueTV = new TextView(getContext());
                    valueTV.setText(resp[i] + " " + resp[i+1]);
                    valueTV.setTextSize(40);
                    final String username = resp[i+2];
                    valueTV.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v){
                            DataSingleton.getInstance().setSelectedPatient(username);
                            NavHostFragment.findNavController(CarerHomeFragment.this)
                                    .navigate(R.id.carer_to_info_patient);
                        }

                    });
                    binding.listPatient.addView(valueTV);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                return params;
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}