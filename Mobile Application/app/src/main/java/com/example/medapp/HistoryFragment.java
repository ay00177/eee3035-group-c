package com.example.medapp;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.medapp.databinding.FragmentHistoryBinding;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class HistoryFragment extends Fragment {

    private FragmentHistoryBinding binding;
    RequestQueue queue;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        queue = Volley.newRequestQueue(this.getContext());
        binding = FragmentHistoryBinding.inflate(inflater, container, false);
        queue.add(historyRequest(DataSingleton.getInstance().selectedPatient));

        return binding.getRoot();

    }


    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.patientNameHistory.setText(DataSingleton.getInstance().selectedPatient);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
    StringRequest historyRequest(String username) {
        String Url = "https://skinnerserver.ddns.net/eee3035/GetMissedDoses.php";

        return new StringRequest(Request.Method.POST, Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("VOLLEY", response);

                String[] resp = AppUtils.parseResp(response);
                if(resp.length>1)
                for (int i = 0; i < resp.length; i = i + 3) {
                    TextView valueTV = new TextView(getContext());
                    valueTV.setText(resp[i] + " " + resp[i+1] + " " +resp[i+2]);
                    valueTV.setTextSize(30);
                    binding.missedDosesList.addView(valueTV);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                return params;
            }
        };
    }
}