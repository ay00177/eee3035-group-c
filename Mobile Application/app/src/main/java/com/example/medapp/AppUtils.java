package com.example.medapp;

 public class AppUtils {
    static String[] parseResp(String response){
        return response.replaceAll("\\[", "")
                .replaceAll("]", "")
                .replaceAll("\"", "")
                .split(",");
    }

}
