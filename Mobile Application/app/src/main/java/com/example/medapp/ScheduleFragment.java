package com.example.medapp;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.medapp.databinding.FragmentScheduleBinding;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


public class ScheduleFragment extends Fragment {
    private FragmentScheduleBinding binding;
    RequestQueue queue;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        queue = Volley.newRequestQueue(this.getContext());
        binding = FragmentScheduleBinding.inflate(inflater, container, false);
        queue.add(scheduleRequest(DataSingleton.getInstance().username));
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.backCarerScheduleBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(ScheduleFragment.this).popBackStack();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    StringRequest scheduleRequest(String username) {
        String Url = "https://skinnerserver.ddns.net/eee3035/GetSchedule.php";

        return new StringRequest(Request.Method.POST, Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("VOLLEY", response);

                String[] resp = AppUtils.parseResp(response);
                for (int i = 0; i < resp.length; i = i + 4) {
                    switch (resp[i].replace("\"","")) {
                        case "Monday":
                            binding.textView10.setText(resp[i+1] + " " +resp[i+2]);
                            break;
                        case "Tuesday":
                            binding.textView13.setText(resp[i+1] + " " +resp[i+2]);
                            break;
                        case "Wednesday":
                            binding.textView14.setText(resp[i+1] + " " +resp[i+2]);
                            break;
                        case "Thursday":
                            binding.textView22.setText(resp[i+1] + " " +resp[i+2]);
                            break;
                        case "Friday":
                            binding.textView23.setText(resp[i+1] + " " +resp[i+2]);
                            break;
                        case "Saturday":
                            binding.textView24.setText(resp[i+1] + " " +resp[i+2]);
                            break;
                        case "Sunday":
                            binding.textView25.setText(resp[i+1] + " " +resp[i+2]);
                            break;
                    }
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                return params;
            }
        };
    }

}